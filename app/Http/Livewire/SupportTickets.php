<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\SupportTicket;
use App\Models\TicketQueries;
use Illuminate\Support\Facades\Auth;

class SupportTickets extends Component
{
    public $tickets;
    public $issue, $ticket_id, $status, $reply;
    public $isOpen = 0;
    public $isOpenUpdate = 0;
    public $isOpenReply = 0;
    public $loggedInUser;
    public $ref_no;
    public $search = false;

    public function render()
    {
        $this->loggedInUser = Auth::user();
        if($this->search){
            if($this->ref_no == ""){
                $this->tickets = SupportTicket::all();
            }else{
                $this->tickets = SupportTicket::where('ref_no', $this->ref_no)->get();
            }
        }else{
            if(Auth::user()->type == '1'){
                $this->tickets = SupportTicket::all();
            }else{
                $this->tickets = SupportTicket::where('user_id',Auth::user()->id)->get();
            }
        }

        return view('livewire.support-tickets');
    }

    public function create()
    {
        $this->resetInputFields();
        $this->openModal();
    }

    public function openModal()
    {
        $this->isOpen = true;
    }

    public function openStatusModal()
    {
        $this->isOpenUpdate = true;
    }

    public function isOpenReplyModal()
    {
        $this->isOpenReply = true;
    }

    public function closeModal()
    {
        $this->isOpen = false;
    }

    public function closeReplyModal()
    {
        $this->isOpenReply = false;
    }

    private function resetInputFields(){
        $this->issue = '';
    }

    public function store()
    {
        $this->validate([
            'issue' => 'required'
        ]);

        $ref_id = uniqid();

        SupportTicket::updateOrCreate(['id' => $this->ticket_id], [
            'issue' => $this->issue,
            'user_id' => Auth::user()->id,
            'status' => 'new',
            'ref_no' => $ref_id,
            'created_at' => date("Y/m/d"),
        ]);

        session()->flash('message',
            $this->ticket_id ? 'Ticket Updated Successfully.' : 'Ticket Created Successfully. Please use this reference number to track your ticket '.$ref_id);

        $this->closeModal();
        $this->resetInputFields();
    }

    public function edit($id)
    {
        $ticket = SupportTicket::findOrFail($id);
        $this->ticket_id = $id;
        $this->issue = $ticket->issue;

        $this->openModal();
    }

    public function delete($id)
    {
        SupportTicket::find($id)->delete();
        session()->flash('message', 'Ticket Deleted Successfully.');
    }

    public function update($id)
    {
        $ticket = SupportTicket::findOrFail($id);
        $this->ticket_id = $id;
        $this->status = $ticket->status;

        $this->openStatusModal();
    }

    public function query($id)
    {
        $ticket = SupportTicket::findOrFail($id);
        $this->ticket_id = $id;
        $this->issue = $ticket->issue;

        $this->isOpenReplyModal();
    }

    public function storeReply()
    {
        TicketQueries::updateOrCreate(['id' => $this->ticket_id], [
            'ticket_id' => $this->ticket_id,
            'user_id' => Auth::user()->id,
            'reply' => $this->reply,
            'created_at' => date("Y/m/d"),
        ]);

        session()->flash('message',
            $this->ticket_id ? 'Reply has been compleated.' : 'Reply has been updated');


        $this->closeReplyModal();
    }

    public function search(){
        $this->search = true;
        $this->render();
    }
}
