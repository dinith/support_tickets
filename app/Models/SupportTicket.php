<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SupportTicket extends Model
{
    use HasFactory;

    protected $fillable = [
        'issue',
        'user_id',
        'status',
        'ref_no'
    ];

    protected $hidden = [
        'user_id',
        'ref_no',
        'status',
    ];
}
