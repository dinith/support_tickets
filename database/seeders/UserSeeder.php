<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Support',
            'email' => 'admin@gmail.com',
            'password' => '12345678',
            'type' => '1',
            'contact' => '-',
        ]);
    }
}
