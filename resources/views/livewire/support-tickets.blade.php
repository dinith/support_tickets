<x-slot name="header">
</x-slot>
<div class="py-12">
    <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
        <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg px-4 py-4">
            @if (session()->has('message'))
                <div class="bg-teal-100 border-t-4 border-teal-500 rounded-b text-teal-900 px-4 py-3 shadow-md my-3" role="alert">
                  <div class="flex">
                    <div>
                      <p class="text-sm">{{ session('message') }}</p>
                    </div>
                  </div>
                </div>
            @endif
            <div class="">
                <form>
                    <input type="text" class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" id="exampleFormControlInput1" placeholder="Reference Number" wire:model="ref_no">
                    <button wire:click.prevent="search()" class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded my-3">Find Ticket</button>
                </form>
            </div>
            @if ($loggedInUser->type == '2')
            <button wire:click="create()" class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded my-3">Create New Ticket</button>
            @endif
            @if($isOpen)
                @include('livewire.create')
            @endif

            @if($isOpenUpdate)
                @include('livewire.status')
            @endif

            @if($isOpenReply)
                @include('liveWire.reply')
            @endif
            <table class="table-fixed w-full">
                <thead>
                    <tr class="bg-gray-100">
                        <th class="px-4 py-2">Reference No</th>
                        <th class="px-4 py-2">Issue</th>
                        <th class="px-4 py-2">Status</th>
                        <th class="px-4 py-2">Actions</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($tickets as $post)
                    <tr>
                        <td class="border px-4 py-2">{{ $post->ref_no }}</td>
                        <td class="border px-4 py-2">{{ $post->issue }}</td>
                        <td class="border px-4 py-2">{{ $post->status }}</td>
                        @if ($loggedInUser->type == '1')
                        <td class="border px-4 py-2">
                            <button wire:click="query({{ $post->id }})" class="bg-green-500 hover:bg-green-700 text-white font-bold py-2 px-4 rounded">Reply</button>
                            <button wire:click="update({{ $post->id }})" class="bg-yellow-500 hover:bg-yellow-700 text-white font-bold py-2 px-4 rounded">Update</button>
                        </td>
                        @else
                        <td class="border px-4 py-2">
                            <button wire:click="edit({{ $post->id }})" class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded">Edit</button>
                            <button wire:click="delete({{ $post->id }})" class="bg-red-500 hover:bg-red-700 text-white font-bold py-2 px-4 rounded">Delete</button>
                        </td>
                        @endif
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
